import java.util.Scanner;

public class Own_Fahrkartenautomat {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner tastatur = new Scanner(System.in);
	      
	       double zuZahlenderBetrag; 
	       double eingezahlterGesamtbetrag;
	       double eingeworfeneM�nze;
	       double r�ckgabebetrag;
	       int ticketcount;
	       
	       
	       System.out.print("Ticketpreis Einzelticket(EURO): ");
	       zuZahlenderBetrag = tastatur.nextDouble();
	       
	       System.out.println("Wieviele Fahrkarten ben�tigen Sie?");
	       ticketcount= tastatur.nextInt();
	       System.out.println("Anzahl der Tickets: "+ticketcount);
	       
	       zuZahlenderBetrag=zuZahlenderBetrag*ticketcount;
	       System.out.printf("Zu zahlender Betrag: %.2f Euro \n", zuZahlenderBetrag);
	       
	       // Geldeinwurf
	       // -----------
	       eingezahlterGesamtbetrag = 0.0;
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.printf("\nNoch zu zahlen: %.2f Euro \n" , (zuZahlenderBetrag - eingezahlterGesamtbetrag), "Euro");
	    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2.00 Euro): ");
	    	   eingeworfeneM�nze = tastatur.nextDouble();
	    	   
	    	   if(eingeworfeneM�nze<0.05) {
	    		   System.out.println("Die eingeworfene M�nze kann nicht angenommen werden. ");
	    	   }	
	    			else if(eingeworfeneM�nze>2) {
	    				System.out.println("Dieser Fahrkartenautomat nimmt leider keine Scheine an. ");	
	    			}
	    	
	    			else {
	    				eingezahlterGesamtbetrag += eingeworfeneM�nze;
	    			}
	       }

	       // Fahrscheinausgabe
	       // -----------------
	       System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");

	       // R�ckgeldberechnung und -Ausgabe
	       // -------------------------------
	       r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       if(r�ckgabebetrag > 0.0)
	       {
	    	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro " ,r�ckgabebetrag);
	    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

	           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
	           {
	        	  System.out.println("2.00 EURO");
		          r�ckgabebetrag -= 2.0;
	           }
	           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1.00 EURO");
		          r�ckgabebetrag -= 1.0;
	           }
	           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
		          r�ckgabebetrag -= 0.5;
	           }
	           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	 	          r�ckgabebetrag -= 0.2;
	           }
	           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
		          r�ckgabebetrag -= 0.1;
	           }
	           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	 	          r�ckgabebetrag -= 0.05;
	           }
	       }   
	       
	       tastatur.close();
	       

	       System.out.println("\nVergessen Sie nicht, den/die Fahrschein/e\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir w�nschen Ihnen eine gute Fahrt.");
	     
	      
	    /*Kommentare: Mehrere Tickets k�nnen gekauft werden f�r den selben betrag, zb wenn man in Gruppen f�hrt
	     * 
	     * zuk�nftige Verbesserungen: mehrere Tickets kaufen f�r unterschiedlichen preis, preisklassen einbauen-> man gibt nicht den Preis sondern zb das Tarifgebiet ein
	     * Maximale/minimale M�nzenwerte werden nicht umgesetzt // jetzt schon
	     * 
	     * 5. Datentyp int f�r Fahrkarten, da man nur ganze Karten kaufen kann
	     * 6. die Anzahl der Tickets wird mit dem Preis eines einzelnen Tickets multipliziert, so erh�lt man den zu zahlenden Gesamtpreis
	     */
	}

}
