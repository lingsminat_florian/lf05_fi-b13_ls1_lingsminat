import java.util.Scanner;

public class AB_Schleifen2_million {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		String loop;
		
		//body
		do {
			System.out.println("Wieviel Geld wollen Sie einlagern?");
			double einlage = tastatur.nextDouble();
			System.out.println("Zu welchem Zinssatz in %?");
			double zinssatz = tastatur.nextDouble();
			int jahr = 0;
			
			while(einlage < 1000000) {
				einlage = einlage + einlage * zinssatz/100;
				//System.out.println(einlage);
				jahr++;
			}
			System.out.println("Sie haben "+ jahr +" Jahre gebraucht um Million�r zu werden.");
			
			System.out.println("Wollen sie den Vorgang wiederholen? (ja/nein)");
			loop = tastatur.next();
		}
		while (loop.contains("ja"));
		 //Schluss
		System.out.println("Programm Ende");
		tastatur.close();
	}

}
