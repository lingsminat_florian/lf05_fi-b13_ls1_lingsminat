import java.util.Scanner;

public class AB_Auswahlstrukturen_bmi {

	public static void main(String[] args) {
	
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Geben Sie Ihr Gewicht in kg an.");
		double gewicht = tastatur.nextDouble();
		
		System.out.println("Geben Sie Ihre Größe in cm an.");
		double groesse = tastatur.nextDouble();
		
		double umrechnung = groesse/100;
		double bmi;
		bmi = gewicht/(umrechnung*umrechnung);
		
		System.out.println("Geben Sie ihr Geschlecht an. (m/w)");
		String gender = tastatur.next();
		
		System.out.printf( "Ihr BMI lautet: %.2f\n" ,bmi);
		
		if (gender.equals("m")) {
			if(bmi<20) {
				System.out.println("Sie sind untergewichtig");
			}
			else if (bmi<=25 && bmi>=20) {
				System.out.println("Sie sind normalgewichtig");
			}
			else if (bmi>25) {
				System.out.println("Sie sind übergewichtig");
			}
		}
		
		if (gender.equals("w")) {
			if(bmi<19) {
				System.out.println("Sie sind untergewichtig");
			}
			else if (bmi<=24 && bmi>=19) {
				System.out.println("Sie sind normalgewichtig");
			}
			else if (bmi>24) {
				System.out.println("Sie sind übergewichtig");
			}
		}
		else {
			System.out.println("Eingabefehler");
		}
		
		
		tastatur.close();
		
	}

}
