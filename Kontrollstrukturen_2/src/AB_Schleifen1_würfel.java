import java.util.Scanner;

public class AB_Schleifen1_w�rfel {

	public static void main(String[] args) {

		
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Geben Sie die Seitenl�nge des W�rfels an.");
		int laenge = tastatur.nextInt();
		
		for(int i = 1; i<=laenge;i++) {
			System.out.print("*");
		}
		System.out.println("");

		// body
		for(int a = 1; a <= laenge-2; a++) {
			for(int b = 0; b <= laenge; b++) {
				if(b == 0) {
					System.out.print("*");
				} else if(b == laenge-1) {
					System.out.print("*");
				} else {
					System.out.print(" ");
				}
				
			}
			System.out.println("");	
		}
		
		// ende
		if(laenge > 1) {
			for( int h = 1; h <= laenge; h++) {
				System.out.print("*");
			}
		}
		tastatur.close();
		
	}

}
