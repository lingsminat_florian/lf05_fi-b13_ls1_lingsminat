import java.util.Scanner;
public class AB_Auswahlstrukturen_schaltjahr {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie das Jahr ein");
		int jahr = tastatur.nextInt();
		
		if (jahr <= 1582) {
			if (jahr % 4 == 0) {
				System.out.println("Dieses Jahr ist ein Schaltjahr.");
			}
			else System.out.println("Dieses Jahr ist kein Schaltjahr.");
		}
		
		else if (jahr > 1582) {
			if( jahr % 4 == 0 && jahr % 100 !=0 || jahr % 400 == 0) {
				System.out.println("Dieses Jahr ist ein Schaltjahr.");
			}
			else System.out.println("Dieses Jahr ist kein Schaltjahr.");
			
		}
		
		
		tastatur.close();
	}

}
