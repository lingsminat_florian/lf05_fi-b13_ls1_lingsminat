import java.util.Scanner;

public class AB_Fallunterscheidungen_ohm {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
							//die Formel f�r den Wiederstand R=U/I	
		double spannung;		//in Volt 		U
		double stromstaerke;	//in Ampere		I
		double wiederstand;		//in Ohm		R
		char c;	
		
		System.out.println("Welche Einheit soll berechnet werden? (U, I oder R)");
		c = tastatur.next().charAt(0);
		
		switch(c){
			case'U':
				System.out.println("Geben Sie die Stromst�rke in Ampere ein.");
				stromstaerke = tastatur.nextDouble();
				System.out.println("Geben Sie den Wiederstand in Ohm ein.");
				wiederstand = tastatur.nextDouble();
				
				spannung = wiederstand * stromstaerke;
				System.out.println("Die Spannung betr�gt "+spannung+" Volt.");
			break;
			
			case'I':
				System.out.println("Geben Sie die Spannung in Volt ein.");
				spannung = tastatur.nextDouble();
				System.out.println("Geben Sie den Wiederstand in Ohm ein.");
				wiederstand = tastatur.nextDouble();
				
				stromstaerke = spannung / wiederstand;
				System.out.println("Die Stromst�rke betr�gt "+ stromstaerke+ " Ampere.");
			break;
			
			case'R':
				System.out.println("Geben Sie Stromst�rke in Ampere ein.");
				stromstaerke = tastatur.nextDouble();
				System.out.println("Geben Sie die Spannung in Volt ein.");
				spannung = tastatur.nextDouble();
				
				wiederstand = spannung / stromstaerke;
				System.out.println("Der Wiederstand betr�gt "+wiederstand+" Ohm.");
			break;
			
			default: 
				System.out.println("Eingabefehler.");
			break;
		}
		tastatur.close();
	}

}
