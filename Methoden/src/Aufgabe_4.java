
public class Aufgabe_4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		
		System.out.println("------------------------Volumenberechnung------------------------");
			
			VolumenW�rfel(1.0);
			VolumenQuader(1.0, 2.5, 3.0);
		
		double pyramide1 = VolumenPyramide(1.0, 5.0);
		double pyramide2 = VolumenPyramide(2.0, 2.5);
		double pyramide3 = VolumenPyramide(3.5, 2.4);
			System.out.println("Pyramide 1 hat ein Volumen von "+pyramide1+ "Kubikeinheiten.");
			System.out.println("Pyramide 2 hat ein Volumen von "+pyramide2+ "Kubikeinheiten.");
			System.out.println("Pyramide 3 hat ein Volumen von "+pyramide3+ "Kubikeinheiten.");
		
			
			VolumenKugel(4.0);
	}

	
	
	
		public static double VolumenW�rfel(double a) {
			double VW = a*a*a;
			System.out.println("Das Volumen des W�rfels betr�gt: "+VW+" Kubikeinheiten.");
			return VW;
		}
		public static double VolumenQuader(double a, double b, double c) {
			double VQ = a*b*c;
			System.out.println("Das Volumen des Quaders betr�gt: "+VQ+" Kubikeinheiten.");
			return VQ;
		}
		public static double VolumenPyramide(double a, double h) {
			double VP = a*a*h/3;
			return VP;
		}
		public static double VolumenKugel(double r) {
			double VK = 4/3*r*r*r*Math.PI;
			System.out.println("Das Volumen der Kugel betr�gt: "+VK+" Kubikeinheiten.");
			return VK;
		}
}
