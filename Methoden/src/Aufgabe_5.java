import java.util.Scanner;

public class Aufgabe_5 {

	public static void main(String[] args) {
		Scanner reiseGuide = new Scanner(System.in);

		System.out.println("Geben Sie bitte Ihr Reisebudget ein. ");
		System.out.println("HINWEIS: Sie sollten mit einem Budget von �ber 100� verreisen.");
		double budget = reiseGuide.nextDouble();
		
		
	/*	
		  System.out.println("Wollen Sie Geld wechseln? (Ja/Nein)");
		String wechseln= reiseGuide.next();
		if (wechseln=="Ja") {
		
		}
		else if(wechseln=="Nein") {
			System.out.println("Dann halt nicht.");
		}
		else {
			System.out.println("Falsche Eingabe");
		}
	*/	
		while(budget>100) {
			System.out.printf("Ihr Reisebudget betr�gt: %.2f EURO.\n", budget);
			System.out.println("Wieviel Ihres Budgets wollen sie wechseln?");
		
			double wechselbetrag = reiseGuide.nextDouble();
			budget = budget-wechselbetrag;
			
			System.out.println("Ihrem Budget werden "+wechselbetrag+" � abgezogen.");
			System.out.println("Ihr neues Reisebudget betr�gt "+budget+" �.");
			System.out.println("-------------------------------------------------------------------");
		}
		
		System.out.println("Sie sollten umkehren und nach Hause fahren.");
		
		reiseGuide.close();
	}

}
