import java.util.Scanner;

public class Fahrkartenautomat_Methoden {

	public static void main(String[] args) {
	
		 double zuZahlenderBetrag; 
		 double r�ckgabebetrag;
		 String nocheins;
		 
		  Scanner tastatur = new Scanner(System.in);																	//double eingezahlterGesamtbetrag;
		 
		  do {
			  
		  
		 zuZahlenderBetrag = fahrkartenbestellungenErfassen(tastatur);						//wie viele Fahrkarten? //wie teuer die Fahrkarte
		 r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag, tastatur);					//den zu zahlenden gesamtpreis, die while schleife
		 fahrkartenAusgeben();
		 r�ckgeldAusgeben(r�ckgabebetrag);
		  
		  System.out.println("\nVergessen Sie nicht, den/die Fahrschein/e\n"+
                  "vor Fahrtantritt entwerten zu lassen!\n"+
                  "Wir w�nschen Ihnen eine gute Fahrt.");
		  
		  System.out.println("\nM�chten Sie noch ein Ticket kaufen? (ja/nein)");
		  nocheins = tastatur.next();
		 
		  
		  } while (nocheins.equals("ja"));
		  tastatur.close();
	}
	

	
	//Fahrkarten bestellen methode
	public static double fahrkartenbestellungenErfassen(Scanner tastatur) {								//Vor- und Nachteile von Arrays ganz unten
		
		System.out.println("Wie viele Fahrkarten wollen Sie?");
		int count = tastatur.nextInt();
		String s = ""; 
		String ticketnr = "Ticketnummer";
		String bzn = "Bezeichnung";
		String preis = "Preis in Euro";
					
		/*					
		System.out.println("Hier die �bersicht der Tickets:\n");
		System.out.printf("Ticketnummer%-21sBezeichnung%-25sPreis in Euro\n", s, s);
		System.out.println("==================================================================================");
		System.out.printf("1%-32sEinzelfahrschein Berlin AB%-10s2,90\n", s, s);
		System.out.printf("2%-32sEinzelfahrschein Berlin BC%-10s3,30\n", s, s);
		System.out.printf("3%-32sEinzelfahrschein Berlin ABC%-9s3,60\n", s, s);
		System.out.printf("4%-32sKurzstrecke%-25s1,90\n", s, s);
		System.out.printf("5%-32sTageskarte Berlin AB%-16s8,60\n", s, s);
		System.out.printf("6%-32sTageskarte Berlin BC%-16s9,00\n", s, s);
		System.out.printf("7%-32sTageskarte Berlin ABC%-15s9,60\n", s, s);
		System.out.printf("8%-32sKleingruppen-Tageskarte Berlin AB%-3s23,50\n", s, s);
		System.out.printf("9%-32sKleingruppen-Tageskarte Berlin BC%-3s24,30\n", s, s);
		System.out.printf("10%-31sKleingruppen-Tageskarte Berlin ABC%-2s24,90\n\n", s, s);
		*/
		
		//neue Bezeichnungen bitte im Array erg�nzen
		String [] bezeichnungen = new String [10];
		bezeichnungen[0] = "Einzelfahrschein Berlin AB";
		bezeichnungen[1] = "Einzelfahrschein Berlin BC";
		bezeichnungen[2] = "Einzelfahrschein Berlin ABC";
		bezeichnungen[3] = "Kurzstrecke";
		bezeichnungen[4] = "Tageskarte Berlin AB";
		bezeichnungen[5] = "Tageskarte Berlin BC";
		bezeichnungen[6] = "Tageskarte Berlin ABC";
		bezeichnungen[7] = "Kleingruppen-Tageskarte Berlin AB";
		bezeichnungen[8] = "Kleingruppen-Tageskarte Berlin BC";
		bezeichnungen[9] = "Kleingruppen-Tageskarte Berlin ABC";
		
		//neue Preise bitte im Array erg�nzen
		double[] preise = new double[10];
		preise[0] = 2.90;
		preise[1] = 3.30;
		preise[2] = 3.60;
		preise[3] = 1.90;
		preise[4] = 8.60;
		preise[5] = 9.00;
		preise[6] = 9.60;
		preise[7] = 23.50;
		preise[8] = 24.30;
		preise[9] = 24.90;
		
		//�bersicht der Angebote
		System.out.println("Hier die �bersicht der Tickets:\n");
		System.out.printf("%-20s%-40s%-20s\n", ticketnr, bzn, preis);
		System.out.println("==================================================================================");
		for(int i = 0; i < bezeichnungen.length; i++) {
			System.out.printf("%-20s%-40s%.2f\n", i+1, bezeichnungen[i],preise[i]);
		}
		
		//Berechnung des Preises
		System.out.println("\nBitte geben Sie die gew�nschte Ticketnummer ein.");
		int Ticketnummer = tastatur.nextInt();
		double zuZahlenderBetrag = preise[Ticketnummer-1]*count;
		System.out.printf("Zu zahlender Betrag: %.2f Euro \n", zuZahlenderBetrag);
		
		return zuZahlenderBetrag;
	}
	
	
	
	
	//Fahrkarten bezahlen Methode
	public static double fahrkartenBezahlen(double zuZahlenderBetrag, Scanner tastatur) {
		
		double eingeworfeneM�nze;
		double eingezahlterGesamtbetrag;
		
		eingezahlterGesamtbetrag = 0.0;
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.printf("\nNoch zu zahlen: %.2f Euro \n" , (zuZahlenderBetrag - eingezahlterGesamtbetrag), "Euro");
	    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2.00 Euro): ");
	    	   eingeworfeneM�nze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneM�nze;
	       }
		
		double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;	
		return r�ckgabebetrag;
	}
	
	
	
	
	// Fahrkarte ausgeben Methode
	public static void fahrkartenAusgeben() {
		
		  System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 26; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");      
	}
	
	
	//R�ckgeld ausgeben Methode
	public static void r�ckgeldAusgeben(double r�ckgabebetrag) {
		
		  if (r�ckgabebetrag > 0.0)
	       {
	    	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro " ,r�ckgabebetrag);
	    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

	           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
	           {
	        	  System.out.println("2.00 EURO");
		          r�ckgabebetrag -= 2.0;
	           }
	           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1.00 EURO");
		          r�ckgabebetrag -= 1.0;
	           }
	           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
		          r�ckgabebetrag -= 0.5;
	           }
	           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	 	          r�ckgabebetrag -= 0.2;
	           }
	           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
		          r�ckgabebetrag -= 0.1;
	           }
	           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	 	          r�ckgabebetrag -= 0.05;
	           }
	       }   
		  
		  
	}
	
	
	/*
	 * Vorteile von Arrays: Sie machen den Code �bersichtlicher, man muss an weniger Stellen etwas korrigieren 
	 * (glaub das hei�t modularisierung), man kann leichter auf die einzelnen Werte zugreifen da diese alle jeweils
	 * einen festen Platz haben
	 * Nachteile: Arrays sind komplexer als einfache Zuweisungen -> Fehler sind schwerer auszumachen und zu beheben
	 */
	
	
	
	
	
}
