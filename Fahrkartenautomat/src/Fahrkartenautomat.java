﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
       int ticketcount;
       
       
       System.out.print("Ticketpreis Einzelticket(EURO): ");
       zuZahlenderBetrag = tastatur.nextDouble();
       
       System.out.println("Wieviele Fahrkarten benötigen Sie?");
       ticketcount= tastatur.nextInt();
       System.out.println("Anzahl der Tickets: "+ticketcount);
       
       zuZahlenderBetrag=zuZahlenderBetrag*ticketcount;
       System.out.printf("Zu zahlender Betrag: %.2f Euro \n", zuZahlenderBetrag);
       
       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   System.out.printf("\nNoch zu zahlen: %.2f Euro \n" , (zuZahlenderBetrag - eingezahlterGesamtbetrag), "Euro");
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2.00 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rückgabebetrag > 0.0)
       {
    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro " ,rückgabebetrag);
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2.00 EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1.00 EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
       }   
       
       tastatur.close();
       

       System.out.println("\nVergessen Sie nicht, den/die Fahrschein/e\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
     
      
    /*Kommentare: Mehrere Tickets können gekauft werden für den selben betrag, zb wenn man in Gruppen fährt
     * 
     * zukünftige Verbesserungen: mehrere Tickets kaufen für unterschiedlichen preis, preisklassen einbauen-> man gibt nicht den Preis sondern zb das Tarifgebiet ein
     * Maximale/minimale Münzenwerte werden nicht umgesetzt
     * 
     * 5. Datentyp int für Fahrkarten, da man nur ganze Karten kaufen kann
     * 6. die Anzahl der Tickets wird mit dem Preis eines einzelnen Tickets multipliziert, so erhält man den zu zahlenden Gesamtpreis
     */
    
    }
}