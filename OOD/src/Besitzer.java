
public class Besitzer {

	//Attribute
	private String vorname;
	private String name;
	private Konto konto1;
	private Konto konto2;
	
	//Konstruktor
	Besitzer(String vorname, String name, Konto konto1, Konto konto2){
		this.vorname = vorname;
		this.name = name;
		this.konto1 = konto1;
		this.konto2 = konto2;
	}
		
	//Methoden
		public void setVorname(String vorname) {
			this.vorname = vorname;
		}
		public void setName(String name) {
			this.name = name;
		}
		public void setKonto1(Konto konto1) {
			this.konto1 = konto1;
		}
		public void setKonto2(Konto konto2) {
			this.konto2 = konto2;
		}
		
		public String getVorname() {
			return this.vorname;
		}
		public String getName() {
			return this.name;
		}
		public Konto getKonto1() {
			return this.konto1;
		}
		public Konto getKonto2() {
			return this.konto2;
		}
		
		public void gesamtUebersicht() {
			System.out.println("\nKonto 1: " + 
							   "\nIBAN:	" + konto1.getIBAN() +
							   "\nKontonummer: "+ konto1.getKontonr() +
							   "\nKontostand: "+ konto1.getKontostand() +
							   "\n========================================="+
							   "w\nKonto 2: " + 
							   "\nIBAN:	" + konto2.getIBAN() +
							   "\nKontonummer: "+ konto2.getKontonr() +
							   "\nKontostand: "+ konto2.getKontostand());
		}
		public double gesamtVermoegen() {
			return konto1.getKontostand()+konto2.getKontostand();
		}
		
		
		
}


