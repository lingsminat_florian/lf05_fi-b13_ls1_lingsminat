
public class Konto {

	//Attribute
	private String iban;
	private String kontonr;
	private double kontostand;

	//Konstruktor
	Konto(String iban){
		this.iban = iban;
	}
	
	//Methoden
	public void setIBAN(String iban)
	{
	this.iban = iban;
	}
	public String getIBAN()
	{
	return this.iban;
	}
	public void setKontonr(String kontonr)
	{
	this.kontonr = kontonr;
	}
	public String getKontonr()
	{
	return this.kontonr;
	}
//	
	public void setKontostand(double kontostand)
	 
	{
	this.kontostand = kontostand;
	}
//	
	public double getKontostand()
	{
	return this.kontostand;
	}
	public void geldAbheben(double betrag) {
		this.kontostand = this.kontostand - betrag;
	}
	public void geldEinzahlen(double betrag) {
		this.kontostand = this.kontostand + betrag;
	}
	public void geldUeberweisen(double betrag, Konto konto) {
		this.kontostand = this.kontostand - betrag;
		konto.geldEinzahlen(betrag);
	}
	
	
	
	
	
}

