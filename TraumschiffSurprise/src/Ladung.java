
public class Ladung {
	
		//Attribute
		private String bezeichnung;
		private int menge;
	
		//Konstruktor
		Ladung(){};	
		Ladung(String bezeichnung, int menge){
			this.bezeichnung = bezeichnung;
			this.menge = menge;
		}
	
		//Methoden
		public void setBezeichnung(String bezeichnung) {
			this.bezeichnung = bezeichnung;
		}
		public void setMenge(int menge) {
			this.menge = menge;
		}
		public String getBezeichnung() {
			return this.bezeichnung;
		}
		public int getMenge() {
			return this.menge;
		}

}

