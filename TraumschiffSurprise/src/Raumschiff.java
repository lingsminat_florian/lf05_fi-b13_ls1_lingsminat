
public class Raumschiff {

	//Attribute
	private String name;
	private int energie;
	private int schild;
	private int vitalsysteme;
	private int huelle;
	private int photonentorpedos;
	private int androiden;
	private Ladung ladung1;
	private Ladung ladung2;
	private Ladung ladung3;
	//Ladungsverzeichnis[] = new Ladungsverzeichnis[1];
	
	
	//Konstruktor
	Raumschiff(){}
	Raumschiff(String name, int energie, int schild, int vitalsysteme, int huelle, int photonentorpedos, int androiden){
		this.name = name;
		this.energie = energie;
		this.schild = schild;
		this.vitalsysteme = vitalsysteme;
		this.huelle = huelle;
		this.photonentorpedos = photonentorpedos;
		this.androiden = androiden;
		
	}
	Raumschiff(String name, int energie, int schild, int vitalsysteme, int huelle, int photonentorpedos, int androiden, Ladung ladung1){
		this.name = name;
		this.energie = energie;
		this.schild = schild;
		this.vitalsysteme = vitalsysteme;
		this.huelle = huelle;
		this.photonentorpedos = photonentorpedos;
		this.androiden = androiden;
		this.ladung1 = ladung1;
	}
	Raumschiff(String name, int energie, int schild, int vitalsysteme, int huelle, int photonentorpedos, int androiden, Ladung ladung1, Ladung ladung2){
		this.name = name;
		this.energie = energie;
		this.schild = schild;
		this.vitalsysteme = vitalsysteme;
		this.huelle = huelle;
		this.photonentorpedos = photonentorpedos;
		this.androiden = androiden;
		this.ladung1 = ladung1;
		this.ladung2 = ladung2;
	}
	Raumschiff(String name, int energie, int schild, int vitalsysteme, int huelle, int photonentorpedos, int androiden, Ladung ladung1, Ladung ladung2, Ladung ladung3){
		this.name = name;
		this.energie = energie;
		this.schild = schild;
		this.vitalsysteme = vitalsysteme;
		this.huelle = huelle;
		this.photonentorpedos = photonentorpedos;
		this.androiden = androiden;
		this.ladung1 = ladung1;
		this.ladung2 = ladung2;
		this.ladung3 = ladung3;
	}
	
	//Methoden
	public String getName() { return this.name; }
	public int getEnergie() { return this.energie; }
	public int getSchild() { return this.schild; }
	public int getVitalsysteme() { return this.vitalsysteme; }
	public int getHuelle() { return this.huelle; }
	public int getPhotonentorpedos() { return this.photonentorpedos; }
	public int getAndroiden() { return this.androiden; }
	public Ladung getLadung1() { return this.ladung1; } 
	public Ladung getLadung2() { return this.ladung2; } 
	public Ladung getLadung3() { return this.ladung3; } 
	
	
	public void setName(String name) {this.name = name;}
	public void setEnergie(int energie) {this.energie = energie;}
	public void setSchild(int schild) {this.schild = schild;}
	public void setVitalsysteme(int vitalsysteme) {this.vitalsysteme = vitalsysteme;}
	public void setHuelle(int huelle) {this.huelle = huelle;}
	public void setPhotonentorpedos(int photonentorpedos) {this.photonentorpedos = photonentorpedos;}
	public void setAndroiden(int androiden) {this.androiden = androiden;}
	public void setLadung1(Ladung ladung1) {this.ladung1 = ladung1;}
	public void setLadung2(Ladung ladung2) {this.ladung2 = ladung2;}
	public void setLadung3(Ladung ladung3) {this.ladung3 = ladung3;}
	
	
	
	
}
